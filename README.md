# ![Arch Logo](/archlinux-logo.png) Telepítést segítő leírás és scriptek

[Hivatalos telepítési segédlet](https://wiki.archlinux.org/title/Installation_guide#Boot_the_live_environment "Angol nyelvű segédlet")

---
A leírás, és ez alapján a scriptek is feltételezik az alábbiakat:

- EFI rendszer telepítés. *Felejtsük már el végre az BIOS-t!*
- Önálló rendszert telepítünk. *Felejtsük már el végre a Windows-t!*
- systemd-boot használata rendszerindítóként.  
	*Nem kell grub, nem kell refind.  
	A rendszerindítót pár másodpercig látod. Tényleg erőforrást akarsz pazarolni a csicsázásra?*


Ne ugorj fejest a dolgokba!
Nézd át a szkripteket, mielőtt ész nélkül futtatnád!
---
### A Live rendszer indulását követően

Billentyűzet beállítása magyarra.  
A parancs beírásánál figyelj oda, hogy a 'z' és az 'y' billentyű fel van cserélve.

```bash
loadkeys hu
```

Ahhoz, hogy jelen leírás és a kapcsoldó szkriptek használhatóak legyenek, a live rendszerben leklónozzuk a tárolót a gyökérkönyvtár **archsetup** almappájába.

```bash
pacman -Sy --needed git
git clone https://gitlab.com/SzzSHUN/archsetup.git /archsetup
```

---
*Első feltöltés: 2021-05-23*
